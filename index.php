<?php

echo " \n";

echo "********************************************************************************************************************* \n";
echo "Escribir un método que cuente el número de 2s (dos) que aparecen en todos los números entre el 0 y N (inclusive). \n";
echo "********************************************************************************************************************* \n";

/** Establecemos el rango de números */
$initialNumber = 0;
$finalNumber = 102;

$result = 0;

/** Recorremos desde el número inicial hasta el final (inclusive) para obtener las ocurrencias del número 2 */
for ($i = $initialNumber; $i <= $finalNumber; $i++) {
    $result += substr_count(strval($i), "2");
}

echo " \n";

echo ("Número inicial: $initialNumber \n");
echo ("Número final: $finalNumber \n");
echo ("--------------------------- \n");
echo ("Resultado: $result ocurrencia(s)");

echo " \n";
